<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Redis;
use GuzzleHttp\Client;
use phpQuery;

class ScrapeUrls implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct(protected $jobId)
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $id = $this->jobId;
        $jobJson = Redis::get("job:$id");
        $jobData = json_decode($jobJson, true);
        
        $this->scrape($jobData);
    }

    public function scrape(array $jobData)
    {
        $client = new Client();
        $response = $client->get($jobData['url']); 
        $content = (string) $response->getBody();

        $html = phpQuery::newDocument($content);
        
        $scrapedHtml = [];
        $scrapeDataArr = [];
        if($jobData['html_selector'] != ''){
            $scrapedHtmlSelector = $html->find($jobData['html_selector']);

            foreach ($scrapedHtmlSelector as $value) {
                $innerHtml = pq($value)->html();
                $scrapeDataArr[] = $innerHtml;
            }
        }
        
        if($jobData['css_selector'] != ''){
            $scrapedCssSelector = $html->find($jobData['css_selector']);

            foreach ($scrapedCssSelector as $value) {
                $innerHtml = pq($value)->html();
                $scrapeDataArr[] = $innerHtml;
            }
        }

        $id = $jobData['id'];
        $jobData['status'] = 'Complete';
        // Store scraped data as piped delimited string of each
        $jobData['scraped_data'] = implode('|', $scrapeDataArr);
        // Update job
        Redis::set("job:$id", json_encode($jobData));

        echo "Scraping Done!" . PHP_EOL;
    }
}
