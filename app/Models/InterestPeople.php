<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class InterestPeople extends Pivot
{
    use HasFactory;

    protected $table = 'interest_people';

    protected $fillable = [
        'people_id',
        'interest_id'
    ];
}
