<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePeopleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|min:2',
            'surname' => 'required|min:2',
            'email' => 'required|email',
            'id_number' => 'required|digits:13',
            'mobile_number' => 'required|digits:10',
            'date_of_birth' => 'required|date',
            'language_id' => 'required',
            'interests' => 'required',
        ];
    }
}
