<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class People extends Model
{
    use HasFactory, Notifiable;

    protected $fillable = [
        'name',
        'surname',
        'email',
        'id_number',
        'mobile_number',
        'date_of_birth',
        'language_id',
    ];

    public function language ()
    {
        return $this->hasOne(Language::class,'id','language_id');
    }

    public function interests ()
    {
        return $this->belongsToMany(Interest::class);
    }

}
