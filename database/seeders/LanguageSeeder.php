<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\Language;

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $languages = [
            'Afrikaans',
            'English',
            'IsiXhosa',
            'IsiZulu',
        ];

        foreach($languages as $language){
            $check = Language::where(['name' => $language])->first();
            if(empty($check)){
                Language::create([
                    'name' => $language
                ]);
            }
        }
    }
}
