<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreLanguageRequest;
use App\Http\Requests\UpdateLanguageRequest;
use Illuminate\Http\Response;
use App\Models\Language;

class LanguageController extends Controller
{
    /**
     * Display a listing of the resource.
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        try {           
            $lngauages = Language::orderBy('name', 'ASC')
                        ->get();
            return response()->json([
                'error' => false,
                'message' => 'Request successful. Response OK.',
                'code' => Response::HTTP_OK,
                'data' => [
                    'lan' => $lngauages,
                ]
            ],Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage(),
                'code' => $e->getCode(),
            ],500);
        }
    }

    /**
     * Store a newly created resource in storage.
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreLanguageRequest $request)
    {
        $data = $request->all();
        try {           

            Language::create($data);

            return response()->json([
                'error' => false,
                'message' => 'Request successful. Response OK.',
                'code' => Response::HTTP_OK
            ],Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage(),
                'code' => $e->getCode(),
            ], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateLanguageRequest $request, Language $lang)
    {
        $data = $request->all();
        try {           
            
            $lang->update($data);

            return response()->json([
                'error' => false,
                'message' => 'Request successful. Response OK.',
                'code' => Response::HTTP_OK
            ],Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage(),
                'code' => $e->getCode(),
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Language $interest)
    {
        try {           
            $interest->delete();
            return response()->json([
                'error' => false,
                'message' => 'Request successful. Response OK.',
                'code' => Response::HTTP_OK
            ],Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage(),
                'code' => $e->getCode(),
            ], 500);
        }
    }
}
