<?php

namespace App\Http\Controllers;

use App\Models\Interest;
use App\Models\Language;
use App\Models\People;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Get Interests and languages for select options in the front end.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSelectOptions()
    {
        try {           
            $interests = Interest::orderBy('title','ASC')->get();
            $languages = Language::orderBy('name','ASC')->get();
            $people = People::with('language')
                        ->with('interests')
                        ->get();
            return response()->json([
                'error' => false,
                'message' => 'Request successful. Response OK.',
                'code' => Response::HTTP_OK,
                'data' => [
                    'interests' => $interests,
                    'languages' => $languages,
                    'people' => $people,
                ]
            ],Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage(),
                'code' => $e->getCode(),
            ], 500);
        }
    }
}
