<?php

namespace App\Listeners;

use App\Events\PersonCapturedEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Jobs\NotifyPerson;

class PersonCapturedListener
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(PersonCapturedEvent $event): void
    {
        NotifyPerson::dispatch($event->person);
    }
}
