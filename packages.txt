ii  libapache2-mod-php8.1                      8.1.26-1+ubuntu20.04.1+deb.sury.org+1                                amd64        server-side, HTML-embedded scripting language (Apache 2 module)
ii  php-common                                 2:93+ubuntu20.04.1+deb.sury.org+3                                    all          Common files for PHP packages
ii  php-pear                                   1:1.10.13+submodules+notgz+2022032202-2+ubuntu20.04.1+deb.sury.org+1 all          PEAR Base System
ii  php8.1                                     8.1.26-1+ubuntu20.04.1+deb.sury.org+1                                all          server-side, HTML-embedded scripting language (metapackage)
ii  php8.1-cli                                 8.1.26-1+ubuntu20.04.1+deb.sury.org+1                                amd64        command-line interpreter for the PHP scripting language
ii  php8.1-common                              8.1.26-1+ubuntu20.04.1+deb.sury.org+1                                amd64        documentation, examples and common module for PHP
ii  php8.1-curl                                8.1.26-1+ubuntu20.04.1+deb.sury.org+1                                amd64        CURL module for PHP
ii  php8.1-dev                                 8.1.26-1+ubuntu20.04.1+deb.sury.org+1                                amd64        Files for PHP8.1 module development
ii  php8.1-gd                                  8.1.26-1+ubuntu20.04.1+deb.sury.org+1                                amd64        GD module for PHP
ii  php8.1-igbinary                            3.2.15-1+ubuntu20.04.1+deb.sury.org+1                                amd64        igbinary PHP serializer
ii  php8.1-imagick                             3.7.0-4+ubuntu20.04.1+deb.sury.org+2                                 amd64        Provides a wrapper to the ImageMagick library
ii  php8.1-imap                                8.1.26-1+ubuntu20.04.1+deb.sury.org+1                                amd64        IMAP module for PHP
ii  php8.1-intl                                8.1.26-1+ubuntu20.04.1+deb.sury.org+1                                amd64        Internationalisation module for PHP
ii  php8.1-mbstring                            8.1.26-1+ubuntu20.04.1+deb.sury.org+1                                amd64        MBSTRING module for PHP
ii  php8.1-mysql                               8.1.26-1+ubuntu20.04.1+deb.sury.org+1                                amd64        MySQL module for PHP
ii  php8.1-opcache                             8.1.26-1+ubuntu20.04.1+deb.sury.org+1                                amd64        Zend OpCache module for PHP
ii  php8.1-readline                            8.1.26-1+ubuntu20.04.1+deb.sury.org+1                                amd64        readline module for PHP
ii  php8.1-redis                               6.0.2-1+ubuntu20.04.1+deb.sury.org+1                                 amd64        PHP extension for interfacing with Redis
ii  php8.1-soap                                8.1.26-1+ubuntu20.04.1+deb.sury.org+1                                amd64        SOAP module for PHP
ii  php8.1-xml                                 8.1.26-1+ubuntu20.04.1+deb.sury.org+1                                amd64        DOM, SimpleXML, XML, and XSL module for PHP
ii  php8.1-xmlrpc                              3:1.0.0~rc3-6+ubuntu20.04.1+deb.sury.org+2                           amd64        XML-RPC servers and clients functions for PHP
ii  php8.1-zip                                 8.1.26-1+ubuntu20.04.1+deb.sury.org+1                                amd64        Zip module for PHP
ii  pkg-php-tools                              1.38                                                                 all          various packaging tools and scripts for PHP packages
