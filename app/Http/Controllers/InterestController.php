<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreInterestRequest;
use App\Http\Requests\UpdateInterestRequest;
use Illuminate\Http\Response;
use App\Models\Interest;

class InterestController extends Controller
{
    /**
     * Display a listing of the resource.
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        try {           
            $interests = Interest::orderBy('title', 'ASC')
                        ->get();
            return response()->json([
                'error' => false,
                'message' => 'Request successful. Response OK.',
                'code' => Response::HTTP_OK,
                'data' => [
                    'interests' => $interests,
                ]
            ],Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage(),
                'code' => $e->getCode(),
            ],500);
        }
    }

    /**
     * Store a newly created resource in storage.
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreInterestRequest $request)
    {
        $data = $request->all();
        try {           

            Interest::create($data);

            return response()->json([
                'error' => false,
                'message' => 'Request successful. Response OK.',
                'code' => Response::HTTP_OK
            ],Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage(),
                'code' => $e->getCode(),
            ], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateInterestRequest $request, Interest $interest)
    {
        $data = $request->all();
        try {           

            $interest->update($data);

            return response()->json([
                'error' => false,
                'message' => 'Request successful. Response OK.',
                'code' => Response::HTTP_OK
            ],Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage(),
                'code' => $e->getCode(),
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Interest $interest)
    {
        try {           
            $interest->delete();
            return response()->json([
                'error' => false,
                'message' => 'Request successful. Response OK.',
                'code' => Response::HTTP_OK
            ],Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage(),
                'code' => $e->getCode(),
            ], 500);
        }
    }
}
