<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;
use App\Jobs\ScrapeUrls as ScrapingJob;
use GuzzleHttp\Client;
use phpQuery;

class ScrapeUrls extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:scrape-urls';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Start the scraping of all pending jobs';

    /**
     * Execute the console command.
     * Process all jobs of status Pending
     */
    public function handle()
    {
        // Get last job ID
        $lastJobId = Redis::get('job:id');
        
        if($lastJobId){
            // Go through ech job ID and skip Complete and dispatch skryping JOB
            for ($i = 1; $i <= $lastJobId ; $i++) { 
                $jobJson = Redis::get("job:$i");
                $jobData = json_decode($jobJson, true);

                if($jobData['status'] != 'Pending'){
                    continue;
                }
                dispatch(new ScrapingJob($i));
            }
        } else {
            echo "No jobs found! \n";
        }
    }

}
