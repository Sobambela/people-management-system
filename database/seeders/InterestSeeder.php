<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\Interest;

class InterestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $interests = [
            'Programming',
            'Cosmology',
            'Education',
            'Science',
            'Movies',
            'Social Media'
        ];

        foreach($interests as $interest){
            $check = Interest::where(['title' => $interest])->first();
            if(empty($check)){
                Interest::create([
                    'title' => $interest
                ]);
            }
        }
    }
}
