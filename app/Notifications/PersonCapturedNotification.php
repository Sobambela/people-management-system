<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\Models\People;

class PersonCapturedNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     */
    public function __construct(
        public People $person
    ){}

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail(object $notifiable): MailMessage
    {
        return (new MailMessage)
                    ->line('Hi' . $notifiable->name)
                    ->line('Below is your information as captured by ProPay.')
                    ->line('Fullname: ' . $notifiable->name . ' ' . $notifiable->surname)
                    ->line('Email: ' . $notifiable->email)
                    ->line('ID Number: ' . $notifiable->id_number)
                    ->line('Mobile Number: ' . $notifiable->mobile_number)
                    ->line('Birth Date: ' . $notifiable->date_of_birth)
                    ->line('Date Captured: ' . $notifiable->created_at)
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        return [
            //
        ];
    }
}
