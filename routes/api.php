<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\JobsController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/get-languages', [App\Http\Controllers\LanguageController::class, 'index']);
Route::post('/add-language', [App\Http\Controllers\LanguageController::class, 'store']);
Route::post('/update-language/{language}', [App\Http\Controllers\LanguageController::class, 'update']);
Route::delete('/delete-language/{language}', [App\Http\Controllers\LanguageController::class, 'destroy']);

Route::get('/get-interests', [App\Http\Controllers\InterestController::class, 'index']);
Route::post('/add-interest', [App\Http\Controllers\InterestController::class, 'store']);
Route::post('/update-interest/{interest}', [App\Http\Controllers\InterestController::class, 'update']);
Route::delete('/delete-interest/{interest}', [App\Http\Controllers\InterestController::class, 'destroy']);

