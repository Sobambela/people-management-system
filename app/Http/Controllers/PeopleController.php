<?php

namespace App\Http\Controllers;

use App\Events\PersonCapturedEvent;
use App\Http\Requests\StorePeopleRequest;
use App\Http\Requests\UpdatePeopleRequest;
use Illuminate\Http\Response;
use App\Models\People;

class PeopleController extends Controller
{
    /**
     * Display a listing of the resource.
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        try {           
            $people = People::with('language')
                        ->with('interests')
                        ->get();
            return response()->json([
                'error' => false,
                'message' => 'Request successful. Response OK.',
                'code' => Response::HTTP_OK,
                'data' => [
                    'people' => $people,
                ]
            ],Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage(),
                'code' => $e->getCode(),
            ],500);
        }
    }

    /**
     * Store a newly created resource in storage.
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StorePeopleRequest $request)
    {
        $personData = $request->all();
        try {           
            $personData['date_of_birth'] = date('Y-m-d', strtotime($personData['date_of_birth']));
            
            $person = People::create($personData);
            $person->interests()->attach($personData['interests']);

            // Email the person informing them that they have been captured
            // Attempt this only if there appears to be SMTP settings in config(.env)
            if(!empty(config('mail.mailers.smtp.username'))){
                PersonCapturedEvent::dispatch($person);
            }

            return response()->json([
                'error' => false,
                'message' => 'Request successful. Response OK.',
                'code' => Response::HTTP_OK
            ],Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage(),
                'code' => $e->getCode(),
            ], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdatePeopleRequest $request, People $people)
    {
        $personData = $request->all();
        try {           
            $personData['date_of_birth'] = date('Y-m-d', strtotime($personData['date_of_birth']));

            $people->update($personData);
            $people->interests()->sync($personData['interests']);

            return response()->json([
                'error' => false,
                'message' => 'Request successful. Response OK.',
                'code' => Response::HTTP_OK
            ],Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage(),
                'code' => $e->getCode(),
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(People $people)
    {
        try {           
            $people->delete();
            return response()->json([
                'error' => false,
                'message' => 'Request successful. Response OK.',
                'code' => Response::HTTP_OK
            ],Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage(),
                'code' => $e->getCode(),
            ], 500);
        }
    }
}
