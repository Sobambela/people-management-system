<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return redirect()->intended('login'); 
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/get-select-options', [App\Http\Controllers\HomeController::class, 'getSelectOptions'])->name('get-select-options');

Route::post('/add', [App\Http\Controllers\PeopleController::class, 'store'])->name('add-person');
Route::post('/update/{people}', [App\Http\Controllers\PeopleController::class, 'update'])->name('update-person');
Route::get('/delete/{people}', [App\Http\Controllers\PeopleController::class, 'destroy'])->name('delete-person');
