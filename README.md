
## People Manager

### Overview
A web-based system where a user can log in and out of the system to manage people via CRUD operations.

Herewith the fields that are managed: (all fields are required)
- Name
- Surname
- South African Id Number
- Mobile Number
- Email Address
- Birth Date
- Language (Single Selection)
- Interests (Multiple Selection)

On capturing a person: An email is sent out to the person captured informing them that they've been captured on the system by firing an event that triggers a job.

This system uses **Laravel 10x** with **Vue JS**.
Asynchronous processes use **axios**.
The databse is **MySql**.


### System Requirements

The application was developed and test using the environment below:
- **Ubuntu Server 22.04**
- **MySQL Server**
- **Apache2** 
- **PHP 8.1**
- **Composer** 

### Installation and Setup Instructions

#### Clone Source code for local development

The commands provided below can be used as reference, but you will need to change where required to suit your environment and needs
- First, clone the package repository from my github account for local development:

```
$ cd /PATH/TO/WEBAPPS
$ git clone https://gitlab.com/Sobambela/people-management-system.git
```

Run composer install. Then you will need to configure the database information before running the migrations. After you should be ready to serve the application.

```
$  composer install
$ cp .env.example .env
$ php artisan key:generate
```
In the **.env** update the database information to reflect your own.
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=your_database_name
DB_USERNAME=your_db_username
DB_PASSWORD=your_db_password
```
If using a specific Mailing server, update this section of the **.env** file aswell. NB The Sending of the notification will be skipped if this section is left as it is:
```
MAIL_MAILER=smtp
MAIL_HOST=mailpit
MAIL_PORT=1025
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
MAIL_FROM_ADDRESS="hello@example.com"
MAIL_FROM_NAME="${APP_NAME}"
```


```
$ php artisan migrate --seed
$ php artisan serve
```
The database is seeded with a test user, details below, languages and predifined interests.

```
Username: admin@test.com
Password: admin123
```


#### Views

The landing page's  view after login embeds  Vue JS component located in the directory **/resources/js/components**

### Additional Functionality

There is also an API to manage the CRUD for Languages and Interest, the endpoints are outlined below:

#### Get all languages
```
Method: POST 
URI: /api/get-languages
```
#### Add a language
```
Method: POST 
URI: /api/add-language
Payload:
{
    'name': 'Venda'
}
```
#### Update a language
```
Method: POST 
URI: /api/update-language/{language_id}
Payload:
{
    'name': 'Tshivenda'
}
```
#### Delete a language
```
Method: DELETE 
URI: /api/update-language/{language_id}
```

#### Get all interests
```
Method: GET 
URI: /api/get-interests
```
#### Add a interest
```
Method: POST 
URI: /api/add-interest
Payload:
{
    'title': 'Gardenings'
}
```
#### Update a interest
```
Method: POST 
URI: /api/update-interest/{interest_id}
Payload:
{
    'name': 'Gardening'
}
```
#### Delete a interest
```
Method: DELETE 
URI: /api/delete-interest/{interest_id}
```