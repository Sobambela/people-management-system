<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Database\Seeders\InterestSeeder;
use Database\Seeders\LanguageSeeder;
use Database\Seeders\PeopleSeeder;
use Database\Seeders\UserSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {         
        $this->call([
            InterestSeeder::class,
            LanguageSeeder::class,
            UserSeeder::class,
        ]);  
    }
}
